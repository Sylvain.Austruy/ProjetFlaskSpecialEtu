## Installation
***
Tout d'abord forker le projet
***
Ensuite dans la console
```
$ git clone https://gitlab.com/Sylvain.Austruy/ProjetFlaskSpecialEtu.git
```
Si jamais cela ne fonctionne pas essayer
```
$ git clone https://<Pseudo>@gitlab.com/Sylvain.Austruy/ProjetFlaskSpecialEtu.git
```
Création venv :
```
$ cd ProjetFlaskSpecialEtu
$ virtualenv -p python3 venv
$ source venv/bin/activate
```
Installation des packages via requirements puis démarrage de l'application
```
$ pip install -r requirements.txt
$ flask run
```
