import yaml, os.path
from .app import db, login_manager
from flask_login import UserMixin

class Author(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(100))
    
    #Surcharge du ToString d'Author
    def __repr__(self):
        return self.name
        #return "<Id : %d, auteur : %s>"%(self.id, self.name)

class Book(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    price = db.Column(db.Float)
    title = db.Column(db.String(100))
    img = db.Column(db.String(100))
    url = db.Column(db.String(200))
    author_id = db.Column(db.Integer,db.ForeignKey("author.id"))
    author = db.relationship("Author",
        backref = db.backref("books",lazy="dynamic"))

    #Surcharge du ToString de Book
    def __repr__(self):
        #return self.title
        return "Titre : %s, Prix : %f"%(self.title, self.price)


Books = yaml.safe_load(
    open(
        os.path.join(
            os.path.dirname(__file__),
            "data.yml"
        )
    )
)

def get_sample():
    return Books[0:10]

def get_sample_fromBD():
    return Book.query.limit(10).all()

def get_books_between_5_20():
    return Book.query.filter(Book.price > 5).filter(Book.price < 20).order_by(Book.price).all()

def get_books_from_Tolkien():
    return Author.query.filter(Author.name == "J.R.R. Tolkien").one().books.order_by(Book.title).all()

def get_author(id):
    return Author.query.filter(Author.id == id).one()

def NotOnDB(nom):
    a = Author.query.filter(Author.name == nom).one_or_none()
    return a == None

class User(db.Model, UserMixin):
    username = db.Column(db.String(50), primary_key=True)
    password = db.Column(db.String(64))
    def get_id(self):
        return self.username

def get_user(username):
    return User.query.filter(User.username == username).one_or_none()

@login_manager.user_loader
def load_user(username):
    return User.query.get(username)